#include "stdafx.h"
#include "FileDownload.h"

/*********************FileDownload CLASS FUNCTIONS************************/

/*************************************************************************/
/*      FUNCTION NAME: FileDownload::FileDownload (CONSTRUCTOR)          */
/*                                                                       */
/* DESCRIPTION: filling internal variables after object is created       */
/*                                                                       */
/* VARIABLES:       fileUrl      -  URL to download file                 */
/*                  fileName     -  path and name, where downloaded file */
/*                                  is to be stored                      */
/*                                                                       */
/* FUNCTION CALLS:                                                       */
/*                                                                       */
/* RETURN:                                                               */
/*                                                                       */
/*************************************************************************/

FileDownload::FileDownload(char *fileUrl, char *fileName)
{
    cout << "Initialization..." << endl;
    this->url = fileUrl;
    this->outFileName = fileName;
};

/*************************************************************************/
/*         FUNCTION NAME: FileDownload::DownloadFIleFromURL              */
/*                                                                       */
/* DESCRIPTION: download file from url specified in internal variable url*/
/*              and save it into file specified in outFileName  int. var */
/*                                                                       */
/* VARIABLES:                                                            */
/*                                                                       */
/* FUNCTION CALLS: write_data                                            */
/*                                                                       */
/* RETURN:                                                               */
/*                                                                       */
/*************************************************************************/

void FileDownload::downloadFIleFromURL()
{
    CURLcode res;
    CURL *curl;
    FILE *file;

    curl = curl_easy_init();
    if (curl)
    {
        file = fopen(this->outFileName, "wb");
        if(file)
        {
            cout << this->outFileName << " created!" << endl;
            curl_easy_setopt(curl, CURLOPT_URL, this->url);
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
            curl_easy_setopt (curl, CURLOPT_VERBOSE, 1L);
            res = curl_easy_perform(curl);
            cout << "test" << endl;
            curl_easy_cleanup(curl);
            fclose(file);
            cout << "**************************************" << endl;
            cout << "FILE information - enter for continue." << endl;
        }
    }
};

/*************************************************************************/
/*               FUNCTION NAME: FileDownload::fileExists                 */
/*                                                                       */
/* DESCRIPTION: check if file exists yet, if yes then returns true,      */
/*              else return false                                        */
/*                                                                       */
/* VARIABLES:       fileName  - checked file                             */
/*                                                                       */
/* FUNCTION CALLS:                                                       */
/*                                                                       */
/* RETURN: TRUE/FALSE                                                    */
/*                                                                       */
/*************************************************************************/

bool FileDownload::fileExists(const char *fileName)
{
    ifstream infile(fileName);
    return (infile.good());
};

/*************************************************************************/
/*          FUNCTION NAME: FileDownload::replaceExistingFile             */
/*                                                                       */
/* DESCRIPTION: checks if file exists and if so, than replace it or let  */
/*              it based on users decision - y for replace, rest for not */
/*                                                                       */
/* VARIABLES:   fileName - path and name of file which can be replaced   */
/* C:\\Users\\zdenik\\Documents\\GitProjects\\Predictor\\Files\\data.csv */                                                         
/*                                                                       */
/* FUNCTION CALLS: fileExists()                                          */
/*                 downloadFileFromURL()                                 */
/*                                                                       */
/* RETURN:                                                               */
/*                                                                       */
/*************************************************************************/

void FileDownload::replaceExistingFile(const char *fileName)
{
    char answer;
    if (this->fileExists(fileName))
    {
        cout << "File already exists, do you want to replace it?" << endl;
        cout << "('y' for replace, any other for not)" << endl;
        answer = cin.get();
        cin.get();
        if(answer == 'y')
        {
          this->downloadFIleFromURL();
        }
        else
        {
            cout << "Old version let." << endl;
        }
    }
    else
    {
        this->downloadFIleFromURL();
    }
}

/*************************************************************************/
/*              FUNCTION NAME: FileDownload::~FileDownload               */
/*                                                                       */
/* DESCRIPTION: destroying of created object                             */
/*                                                                       */
/* VARIABLES:                                                            */
/*                                                                       */
/* FUNCTION CALLS:                                                       */
/*                                                                       */
/* RETURN:                                                               */
/*                                                                       */
/*************************************************************************/

FileDownload::~FileDownload()
{
    cout << "Removing..." << endl;
};



/****************************GLOBAL FUNCTIONS******************************/

/*************************************************************************/
/*                     FUNCTION NAME: write_data                         */
/*                                                                       */
/* DESCRIPTION: prepare file for writing                                 */
/*                                                                       */
/* VARIABLES:  *ptr - name of file                                       */
/*             size - size of web file                                   */
/*             nmemb - ?                                                 */
/*             stream - variable representing downloaded file            */
/*                                                                       */
/* FUNCTION CALLS:                                                       */
/*                                                                       */
/* RETURN:                                                               */
/*                                                                       */
/*************************************************************************/

size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t written;
    written = fwrite(ptr, size, nmemb, (FILE *)stream);
    return written;
};