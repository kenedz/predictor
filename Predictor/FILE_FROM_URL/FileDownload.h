//FileDownload.h

#include <..\MAIN\stdafx.h>
#include <iostream>
#include <string>
#include <fstream>
#include "curl/curl.h"

using namespace std;

#ifndef _FILEDOWNLOAD_H_
#define _FILEDOWNLOAD_H_

class FileDownload
{
private:
    //variables
    char *url;
    char *outFileName;
public:
     //functions
    FileDownload(char *fileUrl, char *fileName);
    void downloadFIleFromURL();
    bool fileExists(const char *fileName);
    void replaceExistingFile(const char *fileName);
    ~FileDownload();
};

size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream); //size_t - type able to represent the size of any object in bytes

#endif // !1
